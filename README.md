Geometric Primitives dataset. Hopefully to be published at ICVS 2019.

Synthetically generated dataset, which contains five simple-shaped objects and 20 scenes in a bin-picking-scenario.
Scenes were generated with BlenSor, simulating a Kinect v2 with noise equivalent to 1.1% to 1.8% of the model
diameter.

For usage with the PPFM code, place this repository in ~/ROS/datasets/