#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Author: Xaver Kroischke

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
# noinspection PyCompatibility
from future_builtins import *

input = raw_input
range = xrange

# imports
import os
import re
import yaml

from pf_matching.datasets import DatasetBase

DATASET_NAME = 'primitives dataset (Blensor created)'


def get_dataset():
    """
    convenience function for getting an instance of the provider for this dataset
    @return: an instance of the DatasetProvider
    """
    return BlensorDataset()


# noinspection PyMissingOrEmptyDocstring
class BlensorDataset(DatasetBase):
    """
    generic dataset that resulted from Blensor simulations
    """

    def __init__(self):
        self._root_dir = os.path.dirname(os.path.realpath(__file__))  # file location
        self._model_dir = 'models/with_normals'
        self._model_name_regex = '(.*)_with_normals\..{3}'
        self._scenes_dir = 'scenes'
        self._scenes_extension = '.pcd'
        self._scene_name_regex = '(.*)' + self._scenes_extension
        self._gt_dir = 'ground_truth'

        # get all existing models
        # models are stored as (model_name, abs_model_path)
        model_dir_abs = os.path.join(self._root_dir, self._model_dir)
        model_files = [f for f in os.listdir(model_dir_abs)
                       if os.path.isfile(os.path.join(model_dir_abs, f)) and
                       ('.ply' in f.lower() or '.stl' in f.lower())]
        model_files.sort()
        self._models = [(re.search(self._model_name_regex, f).group(1),
                         os.path.join(model_dir_abs, f)) for f in model_files]

        # get all existing scenes
        # scenes are stored as (scene_name, abs_scene_path)
        scene_dir_abs = os.path.join(self._root_dir, self._scenes_dir)
        scene_files = [f for f in os.listdir(scene_dir_abs)
                       if os.path.isfile(os.path.join(scene_dir_abs, f)) and
                       self._scenes_extension.lower() in f.lower()]
        scene_files.sort()
        self._scenes = [(re.search(self._scene_name_regex, f).group(1),
                         os.path.join(scene_dir_abs, f)) for f in scene_files]

    def get_name(self):
        return os.path.basename(os.path.normpath(self._root_dir))

    def get_units(self):
        return 'm'

    def get_model_scale_factor(self):
        return 1 / 1000  # models are in mm, scenes are in m

    def get_number_of_models(self):
        return len(self._models)

    def get_number_of_scenes(self):
        return len(self._scenes)

    def get_model_name_and_path(self, i):
        return self._models[i]

    def get_scene_name_and_path(self, i):
        return self._scenes[i]

    def get_ground_truth(self, model_index, scene_index):
        model_name = self.get_model_name(model_index)
        scene_name = self.get_scene_name(scene_index)

        try:
            with open(os.path.join(self._root_dir, self._gt_dir, scene_name + '.yaml')) as gt_file:
                scene_gt_data = yaml.load(gt_file)

            model_gt_data = scene_gt_data['objects'][model_name]
            return [model_gt_data[i]['pose_matrix'] for i in range(model_gt_data['instances'])]
        except KeyError as e:
            print('Warning: key \'%s\' not found during ground truth retrieval.' % e)
            return None

    def get_degree_of_occlusion(self, model_index, scene_index):
        # Blensor datasets do not provide a degree of occlusion yet
        return None

if __name__ == '__main__':
    test_instance = BlensorDataset()
